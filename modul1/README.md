# MiniX 1
Selve idéen udsprang af årstiden vi har i øjeblikket, hvor jeg begyndte at drømme mig tilbage til de gange der har været masser af sne, i stedet for at det blot er minusgrader udenfor, uden det snebelagte landskab. Dette førte mig videre til idéen om at lave en snemand, for hvis der ikke er sne udenfor til at lave en snemand, så må man jo bare programmere en i stedet for - lige så sjovt:)

Herunder ses det færdige produkt, og hvordan min programmerede snemand endte med at se således ud, vil jeg komme nærmere ind på.
Her er link til min kode:

[https://charlotteoester.gitlab.io/aestetisk-programmering/modul1/index.html]

## Reflektioner undervejs
**//Canvas**

Jeg kunne selvfølgelig ikke programmere min snemand uden at have en baggrund at gøre det på, så jeg valgte at det skulle være 500x500. Til dette benyttede jeg `createCanvas(500,500);`, og for at kunne få baggrunden til en lyseblå farve benyttede jeg `background(110,180,255);`, som jeg kom frem til ved at lege lidt med de forskellige tal. Jeg fandt det en smule udfordrende at finde den perfekte farve medmindre man finder en reference på nettet, men det også spændende og se om man selv kan regne ud hvilke tal man skal bruge til en bestemt farve.


**//Kroppen**

Herefter var næste step at ’bygge’ min snemand. Dette startede ud med, at lave kroppen, som består at 3 ellipser/cirkler. Jeg ønskede derudover at fjerne omridset fra cirklerne, så de smeltede mere sammen, og gav en bedre fornemmelse af, at det var sne jeg forsøgte at efterligne. Dette gjorde jeg med funktionen `noStroke();`.

**//Gulerodsnæsen**

Næsen består blot af en trekant `triangle(250, 175, 250, 195, 160, 185);`, som jeg har farvet orange ved hjælp af funktionen `fill(255,140,0);`. Jeg havde i starten lidt problemer med at hele snemandens krop også blev orange, så for at den orange farve ikke interagerede med snemandens andre bestanddele, brugte jeg også `push();` og `pop();`.

**//Øjne og mund**

Øjnene og munden på snemanden består også blot består af ellipser/cirkler. Cirklerne er identiske udover at størrelsen på øjnenes cirkler er en smule større end mundens cirkler. Jeg brugte igen `push();` og `pop();` for at den sorte farve, `fill(0);`, ikke interagerer med de andre elementer. Det udfordrende ved dette step var, at få cirklerne placeret lige præcis der hvor jeg gerne ville have dem, hvilket jeg opnåede via en form for trial and error. Om det var den bedste løsning ved jeg ikke helt.

**//Arme**

Armene er meget simple, og består af to rektangler `rect(320, 285, 115, 10);`. Her var det den brune farve der drillede mig mest, og var nødt til at søge på nettet for at få en idé om, hvilke tal der skulle bruges hvor. Armene kunne godt have været mere gren-agtige, men pga. tidspres må jeg øve mig på dette en anden dag.

**//Sne**

For at lave sneen i baggrunden krævede det en smule mere tænkning, for at jeg kunne lave et mønster der ikke var alt for tidskrævende, og som så nogenlunde tilfældigt ud, eftersom sne ikke falder i lige linjer. Jeg startede med at lave første række, for at finde ud af hvor meget plads imellem snefnuggene jeg syntes var passende. Næste række ville jeg lave forskudt i forhold til den første, så det så mere ’organisk’ ud, og dette mønster med at skifte mellem de to versioner, benyttede jeg hele vejen igennem, hvilket gjorde det meget simpelt, og jeg behøvede blot at ændre på nogle få tal. Derudover slettede jeg de punkter der var oven på selve snemanden. Der findes sikkert en smartere måde at lave ’sne’ på, men nåede ikke at undersøge det nærmere.

### Generel programmeringsoplevelse

Det er første gang jeg giver mig i kast med at programmere noget som helst, og jeg syntes det har været meget spændende og sjovt, og ikke mindst en smule udfordrende. Der er selvfølgelig altid plads til forbedringer og mange andre ting man kan dykke ned i, men dette var hvad jeg kom frem til, mens jeg sad og legede med de forskellige muligheder for, hvad man kan programmere. Jeg har dog helt klart fået en bredere forståelse for hvad programmering indebærer, og glæder mig til at se hvor det fører hen - og tænker at det kun er fantasien der sætter grænser for, hvad man kan med programmering.






